package com.project.plantappui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.widget.ANImageView;
import com.project.plantappui.R;
import com.project.plantappui.perpus.PerpusModel;

import java.util.ArrayList;

public class PerpusAdapter extends RecyclerView.Adapter<PerpusAdapter.PerpusItemViewHolder> {

    private ArrayList<PerpusModel> listBook;
    private LayoutInflater inflater;
    private Context context;

    public PerpusAdapter(ArrayList<PerpusModel> listBook, Context context) {
        this.listBook = listBook;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public PerpusAdapter.PerpusItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.recommended_item, parent, false);
        final PerpusItemViewHolder perpusItemViewHolder = new PerpusItemViewHolder(rootView);
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position    = perpusItemViewHolder.getAdapterPosition();
                int spId        = listBook.get(position).getKuantitas();
            }
        });
        return perpusItemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PerpusAdapter.PerpusItemViewHolder holder, int position) {
        PerpusModel perpusModel = listBook.get(position);
        holder.ivGambar.setImageUrl(perpusModel.getGambar());
        holder.tvNama.setText(perpusModel.getName());
        holder.tvPenulis.setText(perpusModel.getPenulis());
    }

    @Override
    public int getItemCount() {
        return listBook.size();
    }

    public void setListBook(ArrayList<PerpusModel> listBook) {
        this.listBook = listBook;
    }

    public class PerpusItemViewHolder extends RecyclerView.ViewHolder {
        TextView tvNama;
        ANImageView ivGambar;
        TextView tvPenulis;
        TextView tvPenerbit;
        TextView tvKuantitas;
        TextView tvKuantitasTersedia;
        public PerpusItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ivGambar    = itemView.findViewById(R.id.item_picture);
            tvNama      = itemView.findViewById(R.id.item_recommended_title);
            tvPenulis   = itemView.findViewById(R.id.item_recommended_country);
            tvKuantitas = itemView.findViewById(R.id.item_recommended_price);

        }
    }
}
