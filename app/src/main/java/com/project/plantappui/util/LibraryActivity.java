package com.project.plantappui.util;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;

import com.project.plantappui.R;
import com.stringcare.library.SC;

public class LibraryActivity extends AppCompatActivity {

    private final String TAG = SC.getString(R.string.app_name);
    /**
     * Get access token that have saved to preference
     * @return access token, default null
     */
    public String getAccessToken(){
        return PreferenceManager.newInstance(this).getFromPreference(PreferenceManager.ACCESS_TOKEN);
    }

    /**
     * show error log
     * @param e Exception object
     */
    public void showErrorLog(Exception e){
        Log.e(TAG, "", e);
    }

    /**
     * Hide soft keyboard
     * @param v view that attached to the window screen
     */
    public void hideSoftKeyboard(View v){
        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
