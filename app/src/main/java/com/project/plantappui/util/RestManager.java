package com.project.plantappui.util;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.project.plantappui.R;
import com.stringcare.library.SC;

import org.json.JSONObject;

import java.io.File;
import java.util.Map;

public class RestManager {

    private static RestManager restManager = null;
    private ANRequest.GetRequestBuilder getRequestBuilder;
    private ANRequest.PostRequestBuilder postRequestBuilder;
    private ANRequest.MultiPartBuilder uploadRequestBuilder;

    /**
     * create instance of this class
     * @return this
     */
    public static RestManager newInstance(){
        if(restManager==null){
            restManager = new RestManager();
        }
        return restManager;
    }

    private RestManager(){
    }

    /**
     * request to rest using get method
     * @param url Url to be requested, just relative path url
     * @param headers Header request. It can be null
     * @param queryParameters Query parameter. It can be null
     * @param jsonObjectRequestListener Respond callback in json object format. It can be null
     * @param jsonArrayRequestListener Respond callback in json array format. It can be null
     */
    public void getRequest(String url, Map<String, String> headers, Map<String, String> queryParameters,
                           JSONObjectRequestListener jsonObjectRequestListener, JSONArrayRequestListener jsonArrayRequestListener){
        getRequestBuilder = AndroidNetworking.get(SC.getString(R.string.api_main_url)+url);
        if(headers!=null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                String parameter = entry.getKey();
                String value = entry.getValue();
            }
            getRequestBuilder.addHeaders(headers);
        }
        if(queryParameters!=null) {
            for (Map.Entry<String, String> entry : queryParameters.entrySet()) {
                String parameter = entry.getKey();
                String value = entry.getValue();
            }
            getRequestBuilder.addQueryParameter(queryParameters);
        }
        if(jsonObjectRequestListener!=null) {
            getRequestBuilder.build().getAsJSONObject(jsonObjectRequestListener);
        }
        if(jsonArrayRequestListener!=null){
            getRequestBuilder.build().getAsJSONArray(jsonArrayRequestListener);
        }
    }

    /**
     * request to rest using post method
     * @param url Url to be requested, just relative path url
     * @param headers Header request. It can be null
     * @param bodyParameters Body post parameter. It can be null
     * @param jsonObjectRequestListener Respond callback in json object format. It can be null
     * @param jsonArrayRequestListener Respond callback in json array format. It can be null
     */
    public void postRequest(String url, Map<String, String> headers, Map<String, String> bodyParameters,
                            JSONObjectRequestListener jsonObjectRequestListener, JSONArrayRequestListener jsonArrayRequestListener){
        postRequestBuilder = AndroidNetworking.post(SC.getString(R.string.api_main_url)+url);
        if(headers!=null) {
            postRequestBuilder.addHeaders(headers);
        }
        if(bodyParameters!=null) {
            postRequestBuilder.addBodyParameter(bodyParameters);
        }
        if(jsonObjectRequestListener!=null) {
            postRequestBuilder.build().getAsJSONObject(jsonObjectRequestListener);
        }
        if(jsonArrayRequestListener!=null){
            postRequestBuilder.build().getAsJSONArray(jsonArrayRequestListener);
        }
    }

    /**
     * request to rest using post method
     * @param url Url to be requested, just relative path url
     * @param headers Header request. It can be null
     * @param bodyParameters Body post parameter. It can be null
     * @param jsonObjectRequestListener Respond callback in json object format. It can be null
     * @param jsonArrayRequestListener Respond callback in json array format. It can be null
     */
    public void postRequest(String url, Map<String, String> headers, JSONObject bodyParameters,
                            JSONObjectRequestListener jsonObjectRequestListener, JSONArrayRequestListener jsonArrayRequestListener){
        postRequestBuilder = AndroidNetworking.post(SC.getString(R.string.api_main_url)+url);
        if(headers!=null) {
            postRequestBuilder.addHeaders(headers);
        }
        if(bodyParameters!=null) {
            postRequestBuilder.addJSONObjectBody(bodyParameters);
        }
        if(jsonObjectRequestListener!=null) {
            postRequestBuilder.build().getAsJSONObject(jsonObjectRequestListener);
        }
        if(jsonArrayRequestListener!=null){
            postRequestBuilder.build().getAsJSONArray(jsonArrayRequestListener);
        }
    }

    public void uploadRequest(String url, Map<String, String> headers, Map<String, File> fileParameter,
                              Map<String, String> bodyParameters, JSONObjectRequestListener jsonObjectRequestListener,
                              JSONArrayRequestListener jsonArrayRequestListener){


        uploadRequestBuilder = AndroidNetworking.upload(SC.getString(R.string.api_main_url)+url);
        if(headers!=null) {
            uploadRequestBuilder.addHeaders(headers);
        }
        if(fileParameter!=null){
            uploadRequestBuilder.addMultipartFile(fileParameter);
        }
        if(bodyParameters!=null) {
            uploadRequestBuilder.addMultipartParameter(bodyParameters);
        }
        if(jsonObjectRequestListener!=null) {
            uploadRequestBuilder.build().getAsJSONObject(jsonObjectRequestListener);
        }
        if(jsonArrayRequestListener!=null){
            uploadRequestBuilder.build().getAsJSONArray(jsonArrayRequestListener);
        }
    }


    public void postRequest2(String url, Map<String, String> headers, Map<String, String> bodyParameters,
                             JSONObjectRequestListener jsonObjectRequestListener, JSONArrayRequestListener jsonArrayRequestListener){
        postRequestBuilder = AndroidNetworking.post(SC.getString(R.string.api_main_url_google)+url);
        if(headers!=null) {
            postRequestBuilder.addHeaders(headers);
        }
        if(bodyParameters!=null) {
            postRequestBuilder.addBodyParameter(bodyParameters);
        }
        if(jsonObjectRequestListener!=null) {
            postRequestBuilder.build().getAsJSONObject(jsonObjectRequestListener);
        }
        if(jsonArrayRequestListener!=null){
            postRequestBuilder.build().getAsJSONArray(jsonArrayRequestListener);
        }
    }

    /**
     * request to rest using post method
     * @param url Url to be requested, just relative path url
     * @param headers Header request. It can be null
     * @param bodyParameters Body post parameter. It can be null
     * @param jsonObjectRequestListener Respond callback in json object format. It can be null
     * @param jsonArrayRequestListener Respond callback in json array format. It can be null
     */
    public void postRequest2(String url, Map<String, String> headers, JSONObject bodyParameters,
                             JSONObjectRequestListener jsonObjectRequestListener, JSONArrayRequestListener jsonArrayRequestListener){
        postRequestBuilder = AndroidNetworking.post(SC.getString(R.string.api_main_url_google)+url);
        if(headers!=null) {
            postRequestBuilder.addHeaders(headers);
        }
        if(bodyParameters!=null) {
            postRequestBuilder.addJSONObjectBody(bodyParameters);
        }
        if(jsonObjectRequestListener!=null) {
            postRequestBuilder.build().getAsJSONObject(jsonObjectRequestListener);
        }
        if(jsonArrayRequestListener!=null){
            postRequestBuilder.build().getAsJSONArray(jsonArrayRequestListener);
        }
    }

    public String extractErrorMessage(JSONObject response, String ErrorMessage){
        JSONObject jsonObjectMessage = response.optJSONObject("error");
        if(jsonObjectMessage!=null) {
            JSONObject jsonObjectData = jsonObjectMessage.optJSONObject("data");
            if (jsonObjectData!=null){
                String message = jsonObjectData.optString("message");
                if(message!=null){
                    return message;
                }else{
                    return ErrorMessage;
                }
            }else{
                return ErrorMessage;
            }
        }else{
            return ErrorMessage;
        }
    }


}
