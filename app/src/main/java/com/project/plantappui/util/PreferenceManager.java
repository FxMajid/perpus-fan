package com.project.plantappui.util;

import android.content.Context;

import com.project.plantappui.R;
import com.securepreferences.SecurePreferences;
import com.stringcare.library.SC;


public class PreferenceManager {

    private static final String PREFERENCE_NAME = SC.getString(R.string.pr_hp);
    private static PreferenceManager preferenceManager = null;
    private SecurePreferences securePreferences = null;

    public static PreferenceManager newInstance(Context context){
        if(preferenceManager==null){
            preferenceManager = new PreferenceManager(context);
        }
        return preferenceManager;
    }

    public String getFromPreference(String key){
        return securePreferences.getString(key, null);
    }

    private PreferenceManager(Context context){
        securePreferences = new SecurePreferences(context, KeyIDManager.getKey(), PREFERENCE_NAME);
    }

    public void saveToPreference(String key, String value){
        if (value != null) {
            securePreferences.edit().putString(key, value).apply();
        }
    }


    public static final String ACCESS_TOKEN = SC.getString(R.string.pr_at);
}
