package com.project.plantappui.util;

import java.nio.charset.Charset;
import java.util.UUID;

public class KeyIDManager {
    private static String getUUID(){
        String thisClassName = KeyIDManager.class.getName();
        Charset.availableCharsets();
        UUID uuid = UUID.nameUUIDFromBytes(thisClassName.getBytes(Charset.forName("UTF-8")));
        return uuid.toString();
    }

    public static String getKey(){
        String appId = getUUID();
        if(appId==null){
            return "171719131";
        }else {
            int lenDeviceId = appId.length();
            return appId.substring(lenDeviceId - 8, lenDeviceId - 1);
        }
    }
}
