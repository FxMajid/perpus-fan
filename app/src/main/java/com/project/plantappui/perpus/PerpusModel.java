package com.project.plantappui.perpus;

import android.os.Parcel;
import android.os.Parcelable;

public class PerpusModel implements Parcelable {

    private String name;
    private String penulis;
    private String penerbit;
    private int kuantitas;
    private int kuantiastTersedia;
    private String gambar;

    public String getName() {
        if(name==null || name.equalsIgnoreCase("null")){
            return "";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPenulis() {
        if(penulis==null || penulis.equalsIgnoreCase("null")){
            return "";
        }
        return penulis;
    }

    public void setPenulis(String penulis) {
        this.penulis = penulis;
    }

    public String getPenerbit() {
        if(penerbit==null || penerbit.equalsIgnoreCase("null")){
            return "";
        }
        return penerbit;
    }

    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    public int getKuantitas() {
        return kuantitas;
    }

    public void setKuantitas(int kuantitas) {
        this.kuantitas = kuantitas;
    }

    public int getKuantiastTersedia() {
        return kuantiastTersedia;
    }

    public void setKuantiastTersedia(int kuantiastTersedia) {
        this.kuantiastTersedia = kuantiastTersedia;
    }

    public String getGambar() {
        if(gambar==null || gambar.equalsIgnoreCase("null")){
            return "";
        }
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public PerpusModel(){

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.penerbit);
        parcel.writeString(this.penulis);
        parcel.writeInt(this.kuantitas);
        parcel.writeInt(this.kuantiastTersedia);
        parcel.writeString(this.gambar);
    }

    protected PerpusModel(Parcel in) {
        this.name       = in.readString();
        this.penerbit   = in.readString();
        this.penulis    = in.readString();
        this.kuantitas  = in.readInt();
        this.kuantiastTersedia  = in.readInt();
        this.gambar     = in.readString();
    }

    public static final Parcelable.Creator<PerpusModel> CREATOR = new Parcelable.Creator<PerpusModel>() {
        @Override
        public PerpusModel createFromParcel(Parcel in) {
            return new PerpusModel(in);
        }

        @Override
        public PerpusModel[] newArray(int size) {
            return new PerpusModel[size];
        }
    };

}
