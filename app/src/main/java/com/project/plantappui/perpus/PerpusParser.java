package com.project.plantappui.perpus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PerpusParser {

    private static PerpusParser perpusParser;

    public static PerpusParser newInstance(){
        if (perpusParser==null){
            perpusParser = new PerpusParser();
        }
        return perpusParser;
    }

    private PerpusParser(){

    }

    public ArrayList<PerpusModel> getBook (JSONObject response) throws JSONException{
        ArrayList<PerpusModel> perpusModel  = new ArrayList<>();
        JSONArray jsonArrayPerpus   = response.getJSONArray("data");
        for (int i=0; i<jsonArrayPerpus.length(); i++){
            JSONObject jsonObjectPerpus = jsonArrayPerpus.optJSONObject(i);
            String namaBuku         = jsonObjectPerpus.optString("name");
            String penerbit         = jsonObjectPerpus.optString("penerbit");
            String penulis          = jsonObjectPerpus.optString("penulis");
            int kuantitas           = jsonObjectPerpus.optInt("qty");
            int kuantitasTersedia   = jsonObjectPerpus.optInt("qty_avail");
            String gambar           = jsonObjectPerpus.optString("url_image");

            PerpusModel perpusModels = new PerpusModel();
            perpusModels.setName(namaBuku);
            perpusModels.setPenerbit(penerbit);
            perpusModels.setPenulis(penulis);
            perpusModels.setKuantitas(kuantitas);
            perpusModels.setKuantiastTersedia(kuantitasTersedia);
            perpusModels.setGambar(gambar);
            perpusModel.add(perpusModels);
        }
        return perpusModel;
    }
}
