package com.project.plantappui.menu.home;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import org.greenrobot.eventbus.EventBus;

import com.project.plantappui.MainActivity;
import com.project.plantappui.R;
import com.project.plantappui.adapter.PerpusAdapter;
import com.project.plantappui.model.MessageEvent;
import com.project.plantappui.perpus.PerpusModel;
import com.project.plantappui.perpus.PerpusParser;
import com.project.plantappui.util.LibraryActivity;
import com.project.plantappui.util.PreferenceManager;
import com.project.plantappui.util.RestManager;
import com.project.plantappui.adapter.GroupAdapter;
import com.project.plantappui.model.Group;
import com.project.plantappui.model.Plant;
import com.stringcare.library.SC;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class HomeFragment extends Fragment implements JSONObjectRequestListener {
    private Context mContext;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private GroupAdapter groupAdapter;
    private RestManager restManager;
    private PerpusParser perpusParser;
    private PerpusAdapter perpusAdapter;
    private int limit = 10;
    private int offset = 1;
    public static final String SEARCH_BOOK = "Search_Book";
    private Map<String, String> mapParams = new HashMap<>();
    private ArrayList<PerpusModel> BookList = new ArrayList<>();
    public static final String REFRESH_BOOK = "Refresh_Book";
    private String requestUrl;
    private RecyclerView rvPerpus;
    private ArrayList<Group> groups;
    private ArrayList<Plant> featured_plants;
    private ArrayList<Plant> recommended;
    //todo 0. If you want to add more types of objects, you can easily do so. Check "todo list" (I added it step by step)

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        restManager = RestManager.newInstance();
        perpusParser = PerpusParser.newInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)) {
            // subscribe event
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent messageEvent){
        if(messageEvent.getMessage().equals(SEARCH_BOOK)){
            BookList.clear();
            offset = 0;
            String query = messageEvent.getPayload();
            mapParams.put("string", query);
            requestBook(mapParams);
        }else if(messageEvent.getMessage().equals(REFRESH_BOOK)){
            offset = 0;
            BookList.clear();
            mapParams.clear();
            requestBook(null);
        }
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.fragment_home, container, false);
        rvPerpus = rootView.findViewById(R.id.recyclerView);
        rvPerpus.setLayoutManager(new LinearLayoutManager(getContext()));
        setRvBook();
        requestBook(null);
        
        return rootView;
    }

    private void requestBook(Map<String, String> mapParams) {
        requestUrl = SC.getString(R.string.api_get_books);
        Map<String, String> mapHeaders = new HashMap<>();
        mapHeaders.put("access-token", ((MainActivity) Objects.requireNonNull(getActivity())).getAccessToken());
        restManager.getRequest(requestUrl, mapHeaders, mapParams, this, null);
    }

    /*@Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setAdapterType(view);
        setAdapter();
    }*/

    /*private void initGroupData() {
        groups = new ArrayList<>();
        groups.add(new Group("Recommended", "More"));
        groups.add(new Group("Featured Plants", "More"));
    }*/

    /*private void initPlantData() {
        featured_plants = new ArrayList<>();
        recommended     = new ArrayList<>();

        featured_plants.add(new Plant("Colorado Columbines", "Indonesia", "$300", R.drawable.bottom_img_1));
        featured_plants.add(new Plant("Common Mallows", "Russia", "$200", R.drawable.bottom_img_1));
        featured_plants.add(new Plant("Cherry Blossom", "Italy", "$100", R.drawable.bottom_img_1));

        recommended.add(new Plant("Aquilegia", "Indonesia", "$600", R.drawable.image_1));
        recommended.add(new Plant("Angelica", "Russia", "$500", R.drawable.image_2));
        recommended.add(new Plant("Camellia", "Italy", "$400", R.drawable.image_3));
        recommended.add(new Plant("Narcissa", "France", "$300", R.drawable.image_1));
        recommended.add(new Plant("Orchid", "China", "$200", R.drawable.image_2));
        recommended.add(new Plant("Lily", "America", "$100", R.drawable.image_3));
    }*/

    /*private void setAdapterType(View view) {
        recyclerView    = view.findViewById(R.id.recyclerView);
        layoutManager   = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);
    }*/

    /*private void setAdapter() {
        initGroupData();
        initPlantData();
        //todo 1. Add the new object to the parameter list.
        groupAdapter = new GroupAdapter(mContext, groups, featured_plants, recommended);
        recyclerView.setAdapter(groupAdapter);
    }*/



    @Override
    public void onResponse(JSONObject response) {
        if(requestUrl.equals(SC.getString(R.string.api_get_books))){
            try {
                ArrayList<PerpusModel> thisList = perpusParser.getBook(response);
                BookList.addAll(thisList);
                perpusAdapter.setListBook(thisList);
                perpusAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                Log.e("TAG", "onResponse: ", e);
            }
        }
    }

    private void setRvBook() {
        PerpusAdapter perpusAdapter = new PerpusAdapter(BookList, getContext());
        rvPerpus.setAdapter(perpusAdapter);

    }

    @Override
    public void onError(ANError anError) {

    }
}