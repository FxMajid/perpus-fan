package com.project.plantappui.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputLayout;
import com.project.plantappui.MainActivity;
import com.project.plantappui.R;
import com.project.plantappui.util.LibraryActivity;
import com.project.plantappui.util.PreferenceManager;
import com.project.plantappui.util.RestManager;
import com.stringcare.library.SC;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends LibraryActivity implements JSONObjectRequestListener, View.OnClickListener {

    private String requestUrl;
    private RestManager restManager;
    private PreferenceManager preferenceManager;
    private EditText etUsername;
    private EditText etPassword;
    private TextInputLayout tilUsername;
    private TextInputLayout tilPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        restManager = RestManager.newInstance();
        preferenceManager = PreferenceManager.newInstance(this);

        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);

        tilUsername = findViewById(R.id.til_username);
        tilPassword = findViewById(R.id.til_password);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

    }

    private void callNextPage(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void requestLogin(String username, String password){
        requestUrl  = SC.getString(R.string.api_login_url);
        Map<String, String> mapParams   = new HashMap<>();
        mapParams.put("db", SC.getString(R.string.db));
        mapParams.put("username", username);
        mapParams.put("password", password);
        restManager.postRequest(requestUrl, null, mapParams, this, null);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btn_login){
            hideSoftKeyboard(view);
            String username = etUsername.getText().toString();
            String password = etPassword.getText().toString();
            if(username.isEmpty()){
                tilUsername.setError("Username harus diisi");
            }
            if(password.isEmpty()){
                tilPassword.setError("Password harus diisi");
            }
            if(!username.isEmpty() && !password.isEmpty()){
                tilUsername.setErrorEnabled(false);
                tilPassword.setErrorEnabled(false);
                requestLogin(username, password);
            }
        }

    }

    @Override
    public void onResponse(JSONObject response) {
        if(requestUrl.equals(SC.getString(R.string.api_login_url))) {
            try {
                String accessToken = response.optString("access_token");
                String uid = response.optString("uid");
                String role = response.optString("role");
                String isTdm = response.optString("is_tdm");
                String kategori = response.optString("job_kategori");

                preferenceManager.saveToPreference(PreferenceManager.ACCESS_TOKEN, accessToken);
                callNextPage();


            } catch (Exception e) {
                showErrorLog(e);
            }
        }
    }

    @Override
    public void onError(ANError anError) {

    }
}